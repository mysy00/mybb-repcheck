<?php

if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}

function repcheck_info(): array
{
    return [
        "name" => "Repcheck",
        "description" => "Minimalna ilość reputacji do zakładania wątków",
        "website" => "https://mybboard.pl/",
        "author" => "MyBB PL",
        "authorsite" => "https://mybboard.pl/",
        "version" => "1.0",
        "guid" => "",
        "compatibility" => "*"
    ];
}

function repcheck_is_installed(): int
{
    global $db;

    return $db->num_rows($db->simple_select("settinggroups", "*", "name = \"repcheck\""));
}


function repcheck_install(): void
{
    global $db;

    $settingGroup = [
        "name" => "repcheck",
        "title" => "Repcheck",
        "description" => "Minimalna ilość reputacji do zakładania wątków",
        "isdefault" => "0",
    ];

    $gid = $db->insert_query("settinggroups", $settingGroup);

    $db->insert_query(
        "settings",
        [
            "name" => "repcheck_requiredrep",
            "title" => "Minimum reputacji",
            "description" => "Ile użytkownik musi posiadać reputacji? Domyślnie: 0",
            "optionscode" => "numeric",
            "value" => "0",
            "disporder" => "0",
            "gid" => $gid,
        ]
    );

    $db->insert_query(
        "settings",
        [
            "name" => "repcheck_forums",
            "title" => "Uprawnienia forów",
            "description" => "Jakie fora brać pod uwagę?",
            "optionscode" => "forumselect",
            "value" => "-1",
            "disporder" => "1",
            "gid" => $gid,
        ]
    );

    rebuild_settings();
}

function repcheck_activate(): void
{
    // Intetionally left blank
}

function repcheck_deactivate(): void
{
    global $db;

    $db->delete_query("settinggroups", "name = \"repcheck\"");
    $db->delete_query("settings", "name LIKE \"repcheck%\"");

    rebuild_settings();
}

$plugins->add_hook("datahandler_post_validate_thread", "repcheck");

function repcheck(PostDataHandler $post): void
{
    global $db, $mybb, $forum;

    $requiredReputation = $mybb->settings['repcheck_requiredrep'];

    $userReputation = mysqli_fetch_assoc(
        $db->query(sprintf(
            "SELECT SUM(reputation) as points FROM %sreputation WHERE uid = '%s'",
            TABLE_PREFIX,
            $mybb->user['uid']
        ))
    );

    if ($mybb->settings['repcheck_forums'] == "-1" && $userReputation['points'] > $requiredReputation) {
        return ;
    }

    $forums = explode(",", $mybb->settings['repcheck_forums']);

    if (!in_array($forum['fid'], $forums)) {
        return ;
    }

    $post->is_validated = false;
    $post->set_error(sprintf(
        "Nie posiadasz wystarczająco reputacji, aby zakładać wątki w tym dziale. Wymagana wartość: %s",
        $requiredReputation
    ));
}
